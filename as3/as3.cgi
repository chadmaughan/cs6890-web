#!/usr/bin/perl

use Time::HiRes qw(gettimeofday);

($ss, $susec) = gettimeofday();

print "Content-type:text/html\r\n\r\n";

print '<html>';
print '<head>';
print '<title>CS6890 - Perl CGI (Assignment 3)</title>';
print '</head>';
print '<body>';

print '<b>CS6890 - Perl CGI (Assignment 2)</b><br/><br/>';

# environment variables
print "<b>Environment Variables</b><br/><br/>\n";
print "<table style='font-size: 8pt' width=100%>\n";
foreach (sort keys %ENV) {
	print "<tr><th width=15% align=right>$_</th><td width=85%>$ENV{$_}</td></tr>\n";
}
print "</table><br/><br/>\n";


# query string
print "<b>Query String</b><br/><br/>\n";
print "<table style='font-size: 8pt' width=100%>\n";
if ($ENV{'REQUEST_METHOD'} eq "GET") {

	print "<tr><th width=15% align=right>FULL QUERY STRING</th><td width=85%> " . $ENV{'QUERY_STRING'} . "</td></tr>\n";

    # get name/value pairs
    @pairs = split(/&/, $ENV{'QUERY_STRING'});
    foreach $pair (@pairs) {
		($name, $value) = split(/=/, $pair);
		$value =~ tr/+/ /;
		$value =~ s/%(..)/pack("C", hex($1))/eg;
		print "<tr><th width=15% align=right>$name</th><td width=85%> " . $value . "</td></tr>\n";
    }
}
else {
	print "<tr><td></td><td>NOT GET</td></tr>\n";
}
print "</table><br/><br/>\n";


# post data
print "<b>Post Data</b><br/><br/>\n";
print "<table style='font-size: 8pt' width=100%>\n";
if ($ENV{'REQUEST_METHOD'} eq "POST") {
	read(STDIN, $buffer, $ENV{'CONTENT_LENGTH'});

    # get name/value pairs
    @pairs = split(/&/, $buffer);
    foreach $pair (@pairs) {
		($name, $value) = split(/=/, $pair);
		$value =~ tr/+/ /;
		$value =~ s/%(..)/pack("C", hex($1))/eg;
		print "<tr><th width=15% align=right>$name</th><td width=85%> " . $value . "</td></tr>\n";
    }
}
else {
	print "<tr><td></td><td>NOT POST</td></tr>\n";
}
print "</table><br/><br/>\n";

($es, $uesec) = gettimeofday();
print ($uesec - $susec);

print '</body>';
print '</html>';


1;
