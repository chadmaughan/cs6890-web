//============================================================================
// Name        : as4.cpp
// Author      : Chad Maughan
// Class       : CS6890
//============================================================================

#include <iostream>
#include <fstream>
#include <map>
#include <queue>

#include <signal.h>
#include <pthread.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <stdlib.h>
#include <string.h>
#include <semaphore.h>

using namespace std;

void *reader(void *ptr);
void *counter(void *ptr);
void sighander(int);

sem_t connection_full, connection_empty, log_full, log_empty;
pthread_mutex_t connection_mutex, log_mutex;

queue<int> connection;
queue<int> log;

bool run = true;

map<int, int> tally;
map<int, int>::iterator tt;

// default port
int port = 8000;

void signal_handler(int sig) {
	cout << "signal caught: " << sig << endl;

	// output the tally
	cout << "\nFinal Tally\n-----------\n";
    for (tt = tally.begin(); tt != tally.end(); tt++) {
        cout << tt->first << ": " << tt->second << '\n';
    }

	run = false;
	sleep(3);
	exit(0);
}

int main(int argc, char *argv[]) {

	// get the port from the command line
	if(argc == 2) {
		port = atoi(argv[1]);
		cout << "Command line port set: " << port << endl;
	}

	// catch signals so threads can clean up (prevents file close if you don't)
	signal(SIGABRT, &signal_handler);
	signal(SIGTERM, &signal_handler);
	signal(SIGINT, &signal_handler);

	cout << "Starting server" << endl;
	int limit = 20;

    sem_init(&connection_full, 0, 0);
    sem_init(&connection_empty, 0, limit);

    sem_init(&log_full, 0, 0);
    sem_init(&log_empty, 0, limit);

	// make threads detachable (otherwise they wait for each other to finish)
	pthread_attr_t attr;
	pthread_attr_init(&attr);
	pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_JOINABLE);

	// create readers
	for (int r = 0; r < limit; r++) {
		pthread_t thread;
		pthread_create(&thread, &attr, reader, (void*) r);
	}

	// create counter
	pthread_t counter_thread;
	pthread_create(&counter_thread, &attr, counter, NULL);

	// server socket
	int sock;
	struct sockaddr_in server_addr;

	// server socket
	if ((sock = socket(AF_INET, SOCK_STREAM, 0)) == -1) {
		perror("Socket");
		exit(1);
	}

	server_addr.sin_family = AF_INET;
	server_addr.sin_port = htons(port);
	server_addr.sin_addr.s_addr = INADDR_ANY;
	bzero(&(server_addr.sin_zero), 8);

	// server bind
	if (bind(sock, (struct sockaddr *) &server_addr, sizeof(struct sockaddr)) == -1) {
		perror("Unable to bind");
		exit(1);
	}

	// server listen
	if (listen(sock, 5) == -1) {
		perror("Listen");
		exit(1);
	}

	// listen for connections
	while (run) {

		struct sockaddr_in client_addr;

		// accept
		socklen_t client_size = sizeof(client_addr);
		int connected = accept(sock, (struct sockaddr *) &client_addr, &client_size);

		sem_wait(&connection_empty);
		pthread_mutex_lock(&connection_mutex);

		connection.push(connected);
		printf("connection: %i (%s:%d)\n", connected, inet_ntoa(client_addr.sin_addr), ntohs(client_addr.sin_port));

		pthread_mutex_unlock(&connection_mutex);
		sem_post(&connection_full);

	}

	close(sock);
	sem_destroy(&connection_full);
	sem_destroy(&connection_empty);
	sem_destroy(&log_full);
	sem_destroy(&log_empty);

	return 0;
}

void *reader(void *ptr) {

	int *num = (int *) ptr;
	cout << "created reader: " << num << endl;

	while (run) {

		// remove from connection queue
		if (connection.size() > 0) {
			sem_wait(&connection_full);
			pthread_mutex_lock(&connection_mutex);

			int connected = connection.front();
			cout << "(" << num << ") pulled connection: " << connected << endl;
			connection.pop();

			pthread_mutex_unlock(&connection_mutex);
			sem_post(&connection_empty);

			// receive (expects only a single character)
			char buffer[1];
			bzero(buffer, 1);
			read(connected, buffer, 1);

			int vote = atoi (buffer);
			cout << "(" << num << ") received vote: " << vote << endl;

			// close
			close(connected);

			// add to log queue
			sem_wait(&log_empty);
			pthread_mutex_lock(&log_mutex);
			log.push(vote);
			pthread_mutex_unlock(&log_mutex);
			sem_post(&log_full);
		}
	}

	return 0;
}

void *counter(void *ptr) {

	cout << "running counter thread" << endl;

	// open file
	ofstream log_file("/tmp/vote-tally.txt");

	if (log_file.is_open()) {

		cout << "vote log file created" << endl;
		log_file << "Votes\n-----\n";

		while (run) {

			if (log.size() > 0) {
				// get from the log queue
				sem_wait(&log_full);
				pthread_mutex_lock(&log_mutex);

				int vote = log.front();
				cout << "pulled vote: " << vote << endl;
				log.pop();

				// tally (in memory and write to file)
				log_file << vote;
				log_file << "\n";

				tally[vote]++;

				pthread_mutex_unlock(&log_mutex);
				sem_post(&log_empty);
			}
		}

		// close file
		log_file.close();
	}
	else {
		cout << "Unable to open vote file";
	}

	return 0;
}
