//============================================================================
// Name        : as5.cpp
// Author      : Chad Maughan
//============================================================================

#include <iostream>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>

using namespace std;

string *split(string);

int main(int argc, char * argv[], char *envp[]) {

    struct timeval start, end;
    gettimeofday(&start, NULL);

	cout << "Content-type:text/html\r\n\r\n";

	cout << "<html>";
	cout << "<head>";
	cout << "<title>CS6890 - C++ CGI (Assignment 7)</title>";
	cout << "</head>";
	cout << "<body>";

	cout << "<b>CS6890 - C++ CGI (Assignment 7)</b><br/><br/>" << endl;

	// environment variables
	cout << "<b>Environment Variables</b><br/><br/>" << endl;
	cout << "<table style='font-size: 8pt' width=100%>" << endl;

	for (int n = 0; envp[n]; n++) {

		// split it by '='
		string* parts = split(envp[n]);
		cout << "<tr><th width=15% align=right>" << parts[0] << "</th><td width=85%>" << parts[1] << "</td></tr>\n" << endl;
	}

	cout << "</table><br/><br/>\n";

	string method = getenv("REQUEST_METHOD");

	// GET query string
	cout << "<b>Query String</b><br/><br/>\n";
	cout << "<table style='font-size: 8pt' width=100%>\n";

	if (method.compare("GET")) {
		cout << "<tr><td></td><td>NOT GET</td></tr>\n";
	}
	else {
		cout << "<tr><th width=15% align=right>FULL QUERY STRING</th><td width=85%>" << getenv("QUERY_STRING") << "</td></tr>\n";

		string str = getenv("QUERY_STRING");
		uint cut;
		while ((cut = str.find_first_of("&")) != str.npos) {
			if (cut > 0) {
				// split it by '='
				string* parts = split(str.substr(0, cut));
				cout << "<tr><th width=15% align=right>" << parts[0] << "</th><td width=85%>" << parts[1] << "</td></tr>\n" << endl;
			}
			str = str.substr(cut + 1);
		}

		// get the last part of the query string
		if (str.length() > 0) {
			// split it by '='
			string* parts = split(str.substr(0, cut));
			cout << "<tr><th width=15% align=right>" << parts[0] << "</th><td width=85%>" << parts[1] << "</td></tr>\n" << endl;
		}
	}
	cout << "</table><br/><br/>\n";

	// POST query string
	cout << "<b>Post Data</b><br/><br/>\n";
	cout << "<table style='font-size: 8pt' width=100%>\n";

	if (method.compare("POST")) {
		cout << "<tr><td></td><td>NOT POST</td></tr>\n";
	}
	else {
		string str;
		while (cin) {
			getline(cin, str);
		};

		uint cut;
		while ((cut = str.find_first_of("&")) != str.npos) {
			if (cut > 0) {
				// split it by '='
				string* parts = split(str.substr(0, cut));
				cout << "<tr><th width=15% align=right>" << parts[0] << "</th><td width=85%>" << parts[1] << "</td></tr>\n" << endl;
			}
			str = str.substr(cut + 1);
		}

		// get the last part of the query string
		if (str.length() > 0) {
			// split it by '='
			string* parts = split(str.substr(0, cut));
			cout << "<tr><th width=15% align=right>" << parts[0] << "</th><td width=85%>" << parts[1] << "</td></tr>\n" << endl;
		}
	}
	cout << "</table><br/><br/>\n";

    gettimeofday(&end, NULL);

    cout << "Microseconds: " << (end.tv_usec - start.tv_usec);

	cout << "</body>";
	cout << "</html>";

	return 0;
}

string *split(string line) {

	string static parts[2];

	int i = line.find("=");
	parts[0] = line.substr(0, i);
	parts[1] = line.substr(i + 1, line.length());

	return parts;

}
