#!/usr/bin/python

import cgi, cgitb, time, os
cgitb.enable();

print "Content-type:text/html\r\n\r\n";

print '<html>';
print '<head>';
print '<title>CS6890 - Python CGI (Assignment 7)</title>';
print '</head>';
print '<body>';

print '<b>CS6890 - Python CGI (Assignment 7)</b><br/><br/>';

# environment variables
print "<b>Environment Variables</b><br/><br/>\n";
print "<table style='font-size: 8pt' width=100%%>\n";

for item in os.environ.keys():
	print "<tr><th width=15%% align=right>%s</th><td width=85%%>%s</td></tr>\n" % ( item, cgi.escape( os.environ[ item ] ) );

print "</table><br/><br/>\n";


# query string
print "<b>Query String</b><br/><br/>\n";
print "<table style='font-size: 8pt' width=100%%>\n";

if os.environ['REQUEST_METHOD'] == 'GET':
	print "<tr><th width=15%% align=right>FULL QUERY STRING</th><td width=85%%>%s</td></tr>\n" % ( cgi.escape( os.environ['QUERY_STRING'] ) );

	params = dict([part.split('=') for part in os.environ['QUERY_STRING'].split('&')])

	# get name/value pairs
	for k, v in params.iteritems():
		print "<tr><th width=15%% align=right>%s</th><td width=85%%>%s</td></tr>\n" % (k, v);
else:
	print "<tr><td></td><td>NOT GET</td></tr>\n";

print "</table><br/><br/>\n";


# post data
print "<b>Post Data</b><br/><br/>\n";
print "<table style='font-size: 8pt' width=100%%>\n";

if os.environ['REQUEST_METHOD'] == 'POST':
	for k, v in cgi.FieldStorage().iteritems():
		print "<tr><th width=15%% align=right>%s</th><td width=85%%>%s</td></tr>\n" % (k, v);
#	read(STDIN, $buffer, $ENV{'CONTENT_LENGTH'});
else :
	print "<tr><td></td><td>NOT POST</td></tr>\n";

print "</table><br/><br/>\n";

print '</body>';
print '</html>';

