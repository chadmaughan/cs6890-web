//============================================================================
// Name        : as8.cpp
// Author      : Chad Maughan
// Class       : CS6890
// Description : webserver 2
//============================================================================

#include <iostream>
#include <fstream>
#include <map>
#include <queue>
#include <sstream>
#include <signal.h>
#include <pthread.h>
#include <fcntl.h>
#include <sys/socket.h>
#include <sys/sendfile.h>
#include <arpa/inet.h>
#include <stdlib.h>
#include <stdio.h>
#include <sys/wait.h>
#include <time.h>
#include <unistd.h>
#include <string.h>
#include <semaphore.h>
#include <vector>
#include <sys/types.h>
#include <dirent.h>
#include <errno.h>
#include <vector>
#include <string>
#include <iostream>

using namespace std;

void *reader(void *ptr);
void *logger(void *ptr);
void sighander(int);
void signal_handler(int sig);
string get_file(string header);
string get_request_method(string request);
string get_query_string(string resource);
int get_directory_listing(string dir, vector<string> &files);
string get_content_type(string file);

sem_t connection_full, connection_empty, log_full, log_empty;
pthread_mutex_t connection_mutex, log_mutex;

queue<int> connection;
queue<string> log;

char cwd[FILENAME_MAX];

bool run = true;

// default port
int port = 8000;

int main(int argc, char *argv[]) {

	// get the port from the command line
	if(argc == 3) {
		port = atoi(argv[2]);
		cout << "Command line port set: " << port << endl;
	}

	// catch signals so threads can clean up (prevents file close if you don't)
	signal(SIGABRT, &signal_handler);
	signal(SIGTERM, &signal_handler);
	signal(SIGINT, &signal_handler);

	cout << "Starting server" << endl;
	int limit = 20;

	// get the document root (per assignment, current working directory)
	getcwd(cwd, sizeof(cwd));

    sem_init(&connection_full, 0, 0);
    sem_init(&connection_empty, 0, limit);

    sem_init(&log_full, 0, 0);
    sem_init(&log_empty, 0, 100);

	// make threads detachable (otherwise they wait for each other to finish)
	pthread_attr_t attr;
	pthread_attr_init(&attr);
	pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_JOINABLE);

	// create readers
	for (int r = 0; r < limit; r++) {
		pthread_t thread;
		pthread_create(&thread, &attr, reader, (void*) r);
	}

	// create logger
	pthread_t logger_thread;
	pthread_create(&logger_thread, &attr, logger, NULL);

	// server socket
	int sock;
	struct sockaddr_in server_addr;

	// server socket
	if ((sock = socket(AF_INET, SOCK_STREAM, 0)) == -1) {
		perror("Socket");
		exit(1);
	}

	server_addr.sin_family = AF_INET;
	server_addr.sin_port = htons(port);
	server_addr.sin_addr.s_addr = INADDR_ANY;
	bzero(&(server_addr.sin_zero), 8);

	// server bind
	if (bind(sock, (struct sockaddr *) &server_addr, sizeof(struct sockaddr)) == -1) {
		perror("Unable to bind");
		exit(1);
	}

	// server listen
	if (listen(sock, 5) == -1) {
		perror("Listen");
		exit(1);
	}

	// listen for connections
	while (run) {

		struct sockaddr_in client_addr;

		// accept
		socklen_t client_size = sizeof(client_addr);
		int connected = accept(sock, (struct sockaddr *) &client_addr, &client_size);

		sem_wait(&connection_empty);
		pthread_mutex_lock(&connection_mutex);

		connection.push(connected);
		printf("connection: %i (%s:%d)\n", connected, inet_ntoa(client_addr.sin_addr), ntohs(client_addr.sin_port));

		pthread_mutex_unlock(&connection_mutex);
		sem_post(&connection_full);

	}

	close(sock);
	sem_destroy(&connection_full);
	sem_destroy(&connection_empty);
	sem_destroy(&log_full);
	sem_destroy(&log_empty);

	return 0;
}

/*
 * Reader threads for responding to incoming HTTP requests
 */
void *reader(void *ptr) {

	int *num = (int *) ptr;
	cout << "created reader: " << num << endl;

	while (run) {

		// remove from connection queue
		if (connection.size() > 0) {
			sem_wait(&connection_full);
			pthread_mutex_lock(&connection_mutex);

			int connected = connection.front();
			cout << "(" << num << ") pulled connection: " << connected << endl;
			connection.pop();

			pthread_mutex_unlock(&connection_mutex);
			sem_post(&connection_empty);

			// receive HTTP request
			char buffer[2048];
			read(connected, buffer, 2048);

			cout << "(" << num << ") received request: " << buffer << endl;

			string s(buffer);

			// get the headers
			int i = s.find("\r\n");
			string header = s.substr(0, i);
			cout << "header: " << header << endl;

			// get the requested resource from the header
			string f = get_file(header);
			string of = f;

			// get index.html if they access "/"
			string slash = "/";
			if(f.compare(slash) == 0) {
				f = "/index.html";
			}

			int file = open((cwd + f).c_str(), O_RDONLY);

			// can't find the resource requested
			if(file <= 0) {

				// return directory structure for index resource, 404 for those you can't find
				if(of.compare(slash) == 0) {
					vector<string> files = vector<string>();
					get_directory_listing(cwd, files);
					string output = "<html><body><b>Directory listing for " + of + "</b><br/><br/>";
					for (unsigned int i = 0;i < files.size(); i++) {
						output = output + files[i] + "<br/>";
					}
					output = output + "</body></html>";

					stringstream ss;
					ss << output.length();

					output = "HTTP/1.0 200 OK\r\nContent-Length: " + ss.str() + "\r\nContent-Type: text/html;\r\n\r\n" + output;
					write(connected, output.c_str(), strlen(output.c_str()));
				}
				else {
					string fourofour = "<html><body><b>404</b><br/><br/>Doh! Could not find the resource you requested</body></html>";

					stringstream ss;
					ss << fourofour.length();

					string output = "HTTP/1.0 404 Not found\r\nContent-Length: " + ss.str() + "\r\nContent-Type: text/html;\r\n\r\n" + fourofour;
					write(connected, output.c_str(), strlen(output.c_str()));
				}
			}
			// found the requested resource
			else {

				// check if the requested resource is a CGI script (part of webserver 2 requirements)
				bool cgi = false;

				size_t found = (cwd + f).find("cgi");
				if (found != string::npos) {
					cgi = true;
					cout << "cgi script" << endl;
				}

				if(cgi) {

					int readpipe[2] = { -1, -1 };

					pid_t pID, w;
					int status;

					// create the pipe
					if (pipe(readpipe) == -1) {
						perror("pipe");
						exit(EXIT_FAILURE);
					}

					pID = fork();

					// error
					if (pID == -1) {
						perror("fork");
						exit(EXIT_FAILURE);
					}

					// child
					else if (pID == 0) {

						cout << "child process" << endl;

						// limit the CPU usage to 9 seconds
						struct rlimit lim = {9, 9};
						setrlimit(RLIMIT_CPU, &lim);

						// CGI doesn't expect anything from stdin
						close(readpipe[0]);

						dup2(readpipe[1], 1);
						close(readpipe[1]);

						// environment variables
						char *env[6];
						string env0(cwd, sizeof(cwd));
						env0 = "DOCUMENT_ROOT=" + env0;
						env[0] = strdup(env0.c_str());

						env[1] = strdup("SERVER_SIGNATURE=Chad Maughan's Webserver 2 (CS6890)");

						string env2 = "REQUEST_METHOD=" + get_request_method(header);
						env[2] = strdup(env2.c_str());

						string qs = get_query_string(header);
						string env3 = "QUERY_STRING=" + qs;
						env[3] = strdup(env3.c_str());

						string env4 = "SCRIPT_NAME=" + f;
						env[4] = strdup(env4.c_str());
						env[5] = NULL;

						// cgi cli args
						char *args[1];
						args[0] = NULL;

						// execute cgi
						// 		assume the script directory is (from webserver 1 requirement) [current working directory]/cgi
						execve((cwd + f).c_str(), args, env);

						_exit(EXIT_SUCCESS);
					}

					// parent
					else {

						cout << "parent process" << endl;

						close(readpipe[1]);

						do {

							// get the CGI stdout
							string s = "";
							char buf;
							while (read(readpipe[0], &buf, 1) > 0)
								s += buf;

							stringstream ss;
							ss << s.length();

							string o = "HTTP/1.0 200 OK\r\nContent-Length: " + ss.str() + "\r\n" + s;
							write(connected, o.c_str(), strlen(o.c_str()));

							w = waitpid(pID, &status, WUNTRACED | WCONTINUED);

							if (w == -1) {
								perror("waitpid");
								exit(EXIT_FAILURE);
							}

							if (WIFEXITED(status)) {
								printf("exited, status=%d\n", WEXITSTATUS(status));
							}
							else if (WIFSIGNALED(status)) {
								printf("killed by signal %d\n", WTERMSIG(status));
							}
							else if (WIFSTOPPED(status)) {
								printf("stopped by signal %d\n", WSTOPSIG(status));
							}
							else if (WIFCONTINUED(status)) {
								printf("continued\n");
							}
						}
						while (!WIFEXITED(status) && !WIFSIGNALED(status));
					}

				}
				else {
					ifstream infile((cwd + f).c_str());
					infile.seekg(0, ios::end);
					int fsize = infile.tellg();
					infile.close();

					string content = get_content_type(f);

					stringstream ss;
					ss << fsize;
					string output = "HTTP/1.0 200 OK\r\nContent-Length: " + ss.str() + "\r\nContent-Type: " + content + ";\r\n\r\n";

					write(connected, output.c_str(), strlen(output.c_str()));
					int result = sendfile(connected, file, NULL, fsize);
					if (result == -1)
						perror("sendfile");
				}
			}
			close(file);

			// close
			close(connected);

			// add to log queue
			sem_wait(&log_empty);
			pthread_mutex_lock(&log_mutex);
			log.push(buffer);
			pthread_mutex_unlock(&log_mutex);
			sem_post(&log_full);
		}

		sleep(1);
	}

	return 0;
}

/*
 * Creates a log file of requests handled by server threads
 */
void *logger(void *ptr) {

	cout << "running logger thread" << endl;

	// open file
	ofstream log_file("/tmp/log.txt");

	if (log_file.is_open()) {

		cout << "log file created" << endl;
		log_file << "Log\n----\n";

		while (run) {

			if (log.size() > 0) {

				// get from the log queue
				sem_wait(&log_full);
				pthread_mutex_lock(&log_mutex);

				string request = log.front();
				cout << "pulled request: " << request << endl;
				log.pop();

				// write to file
				log_file << request << "\n";

				pthread_mutex_unlock(&log_mutex);
				sem_post(&log_empty);
			}
		}

		// close file
		log_file.close();
	}
	else {
		cout << "Unable to open log file";
	}

	return 0;
}

/*
 * listens for signals
 */
void signal_handler(int sig) {
	cout << "signal caught: " << sig << endl;
	run = false;
	sleep(3);
	exit(0);
}

/*
 * Gets the file name from the HTTP GET request
 */
string get_file (string file) {

	int i1 = file.find(" ");

	// clear out the query string
	uint q1 = file.find("?");

	string cleanFile;
	if (q1 != string::npos) {
		cleanFile = file.substr(i1+1, (q1 - (i1+1)));
	}
	else {
		cleanFile = file.substr(i1+1, file.length());
	}

	string cleanFile2 = cleanFile;
	uint q2 = cleanFile.find(" ");
	if(q2 != string::npos) {
		cleanFile2 = cleanFile.substr(0, q2);
	}

	return cleanFile2;
}

/*
 * Gets the request method
 */
string get_request_method (string request) {

	uint i1 = request.find(" ");
	string rm;
	if(i1 != string::npos) {
		rm = request.substr(0, i1);
	}
	else {
		rm = "";
	}
	return rm;
}

/*
 * Gets the query string (if it exists)
 */
string get_query_string (string resource) {

	string qs = "";

	uint i1 = resource.find("?");
	if(i1 != string::npos) {
		string qs1 = resource.substr(i1+1, resource.length());

		uint i2 = qs1.find(" ");
		if(i2 != string::npos) {
			qs = qs1.substr(0, i2);
		}
		else {
			qs = qs1;
		}
	}

	cout << "query string: " << qs << endl;
	return qs;
}

/*
 * Populates a list of files if there is not index.html (only index format supported by this webserver)
 */
int get_directory_listing (string dir, vector<string> &files) {
    DIR *dp;
    struct dirent *dirp;
    if((dp  = opendir(dir.c_str())) == NULL) {
        cout << "Error(" << errno << ") opening " << dir << endl;
        return errno;
    }

    while ((dirp = readdir(dp)) != NULL) {
        files.push_back(string(dirp->d_name));
    }
    closedir(dp);
    return 0;
}

/*
 * Gets a content type based on the file extension of the requested resource
 */
string get_content_type (string file) {

	// default content type
	string type = "text/html";
	string extension = "";

	size_t found;
	found = file.rfind(".");
	if (found != string::npos)
		extension = file.substr(found);

	if(extension.compare(".txt") == 0) {
		type = "text/plain";
	}
	else if(extension.compare(".png") == 0) {
		type = "image/png";
	}
	else if(extension.compare(".gif") == 0) {
		type = "image/gif";
	}
	else if(extension.compare(".jpg") == 0) {
		type = "image/jpg";
	}
	else if(extension.compare(".htm") == 0) {
		type = "text/html";
	}

	return type;
}
