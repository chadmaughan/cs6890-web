__author__ = 'chadmaughan'

import web
import os

db_file = os.getcwd() + "/db/final.db"
db = web.database(dbn='sqlite', db=db_file)

class login:
    def GET(self):
        if web.ctx.session.login == 1:
            raise web.seeother('/')
        else:
            
            warning = ""
            if web.ctx.query == "?missing":
                warning = "Email or password missing"
            elif web.ctx.query == "?incorrect":
                warning = "Incorrect user name and/or password"

            return '''
               <!DOCTYPE HTML>
                <html lang="en-US">
                <head>
                    <meta charset="UTF-8">
                    <title>Login</title>
                </head>
                <body>
                    <div id="login">
                        <span class="warning">''' + warning + '''</span>
                        <form action="/login" method="POST">
                            <p><label for="email" id="user-label">Email</label></p>
                            <p><input type="text" id="email" name="email"></p>

                            <p><label for="password" id="password-label">Password:</label></p>
                            <p><input type="password" id="password" name="password"></p>
                            <p><input type=submit value="Login"></p>
                        </form>
                    </div>
                </body>
                </html>'''

    def POST(self):

        inputs = web.input()
        if 'email' in inputs and 'password' in inputs:

            vars = dict(email=inputs.email, password=inputs.password)
            count = db.query("SELECT count(*) as count FROM user WHERE email=$email AND password=$password", vars)

            length = count[0].count
            if length == 1:

                user = db.query("SELECT id, email, first_name, last_name FROM user WHERE email=$email AND password=$password", vars)
                u = user[0]
                web.ctx.session.id = u.id
                web.ctx.session.email = u.email
                web.ctx.session.name = u.first_name + " " + u.last_name
                web.ctx.session.login = 1
                raise web.seeother("/")
            else:
                raise web.seeother("/login?incorrect")
        else:
            raise web.seeother("/login?missing")

        raise web.seeother("/")


class reset:
    def GET(self):
        web.ctx.session.login = 0
        web.ctx.session.kill()
        raise web.seeother('/login')
