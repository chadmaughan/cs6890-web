__author__ = 'chadmaughan'

import os
import web
import json

db_file = os.getcwd() + "/db/final.db"
db = web.database(dbn='sqlite', db=db_file)

class rest:
    def POST(self, id):

        inputs = web.input(file={})

        if 'file' in inputs:

            dir = os.getcwd() + "/static/images"
            path = inputs.file.filename.replace('\\', '/')
            file_name = path.split('/')[-1]

            # check to see if the photo already exists
            name = inputs.name
            vars = dict(image=file_name)
            photo = db.query("SELECT count(*) as count FROM photo WHERE image=$image", vars)

            count = photo[0].count

            # create
            if count == 0:
                out = open(dir + '/' + file_name, 'w')
                out.write(inputs.file.file.read())
                out.close()

                n = db.insert('photo', image=file_name, name=name, user_id=web.ctx.session.id)

        raise web.seeother('/')

    def DELETE(self, id):

        # check to see if the photo exists
        vars = dict(id=id)
        photo = db.query("SELECT count(*) as count FROM photo WHERE id=$id", vars)

        count = photo[0].count

        # not found (send HTTP 404)
        if count == 0:
            result = web.notfound

        # found it (send HTTP 200)
        else:

            where = "id=" + id
            db.delete('photo', where=where)
            result = web.ok

        return result

class list:
    def GET(self, id):

        # check to see if the user exists
        vars = dict(id=id)
        user = db.query("SELECT count(*) as count FROM user WHERE id=$id", vars)

        count = user[0].count

        # not found (send HTTP 404)
        if count == 0:
            return web.notfound

        # found it (return JSON)
        else:
            vars = dict(id=id)
            results = db.select('photo', vars, where="user_id=$id", what="name, image")

            web.header('Content-Type', 'application/json')
            return json.dumps(results.list())