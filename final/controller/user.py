__author__ = 'chadmaughan'

import os
import web
import json

db_file = os.getcwd() + "/db/final.db"
db = web.database(dbn='sqlite', db=db_file)

class rest:
    def POST(self, email):

        inputs = web.input()

        # return HTTP 400 (Bad Request) if they don't have an email and password
        if 'password' in inputs and 'email' in inputs:

            # name parts can be null
            first_name = web.sqlliteral("NULL")
            if 'first_name' in inputs:
                first_name = inputs.first_name
            middle_name = web.sqlliteral("NULL")
            if 'middle_name' in inputs:
                middle_name = inputs.middle_name
            last_name = web.sqlliteral("NULL")
            if 'last_name' in inputs:
                last_name = inputs.last_name

            # check to see if the user already exists
            vars = dict(email=inputs.email)
            user = db.query("SELECT count(*) as count FROM user WHERE email=$email", vars)

            count = user[0].count

            # create
            if count == 0:

                n = db.insert('user', email=inputs.email, password=inputs.password, first_name=first_name, middle_name=middle_name, last_name=last_name)
                result = web.ok

            # more than one user (return HTTP 409 error)
            else:

                result = web.conflict

        else:
            result = web.badrequest

        raise result

    def PUT(self, email):

        inputs = web.input()

        # check to see if the user already exists
        vars = dict(email=email)
        user = db.query("SELECT count(*) as count FROM user WHERE email=$email", vars)

        count = user[0].count

        # update
        if count == 1:

            values = ""
            if 'first_name' in inputs:
                values += "first_name = '" + inputs.first_name + "',"
            if 'middle_name' in inputs:
                values += "middle_name = '" + inputs.middle_name + "',"
            if 'last_name' in inputs:
                values += "last_name = '" + inputs.last_name + "',"
            if 'password' in inputs:
                values += "password = '" + inputs.password + "',"

            values = values[0:-1]

            where = " where email='" + email + "'"

            # don't update the email address (only other fields)
            db.query("UPDATE user SET " + values + where)
            result = web.ok

        # more than one user (return HTTP 409 error)
        else:
            result = web.notfound

        raise result
        
    def GET(self, email):

        # check to see if the user exists
        vars = dict(email=email)
        user = db.query("SELECT count(*) as count FROM user WHERE email=$email", vars)

        count = user[0].count

        # not found (send HTTP 404)
        if count == 0:
            return web.notfound

        # found it (return JSON)
        else:
            vars = dict(email=email)
            results = db.select('user', vars, where="email = $email", what="first_name, middle_name, last_name, created, email")

            web.header('Content-Type', 'application/json')
            return json.dumps(results[0])

    def DELETE(self, email):

        # check to see if the user exists
        vars = dict(email=email)
        user = db.query("SELECT count(*) as count FROM user WHERE email=$email", vars)

        count = user[0].count

        # not found (send HTTP 404)
        if count == 0:
            result = web.notfound

        # found it (send HTTP 200)
        else:

            where = "email='" + email + "'"
            db.delete('user', where=where)
            result = web.ok

        return result