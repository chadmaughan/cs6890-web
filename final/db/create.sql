-- to create tables, run (from same directory as sql script)
--  sqlite3 final.db
--  sqlite> .read create.sql
--  sqlite> .quit



-- USER table
CREATE TABLE user (
    id INTEGER PRIMARY KEY,
    email VARCHAR(50),
    password VARCHAR(50),
	first_name VARCHAR(50),
    middle_name VARCHAR(50),
    last_name VARCHAR(50),
    created DATE);

CREATE TRIGGER insert_created_date AFTER INSERT ON user
BEGIN
    UPDATE user SET created = DATETIME('NOW') WHERE rowid = new.rowid;
END;

-- insert into user (email, password, first_name, last_name) values ('chadmaughan@gmail.com', 'test', 'Chad', 'Maughan');



-- PHOTO table
CREATE TABLE photo (
    id INTEGER PRIMARY KEY,
    image VARCHAR(255),
    name VARCHAR(100),
    user_id INTEGER,
    FOREIGN KEY(user_id) REFERENCES user(id)
);




-- ANALYTICS table
CREATE TABLE analytics (
    id INTEGER PRIMARY KEY,
    event VARCHAR(50),
    user_id INTEGER,
    FOREIGN KEY(user_id) REFERENCES user(id)
);