__author__ = 'chadmaughan'

import web
import os

db_file = os.getcwd() + "/db/final.db"
db = web.database(dbn='sqlite', db=db_file)

class profile:
    def GET(self):
        if web.ctx.session.login == 0:
            raise web.seeother('/login')
        else:
            return '''
                <!DOCTYPE html>
                <html>
                <head>
                    <script src="http://code.jquery.com/jquery-1.5.js"></script>
                    <link rel="stylesheet" href="static/css/theme.css">
                </head>
                <body>
                    <div id="header">
                        <div id="user">
                            Welcome, <b>''' + web.ctx.session.name + '''</b>
                        </div>
                        <div id="menu">
                            <a href="/">Home</a> |
                            <a href="/profile">Update Profile</a> |
                            <a href="/upload">Add Photo</a> |
                            <a href="/reset">Logout</a>
                        </div>
                    </div>

                    <div class="form">
                        <div>
                            Please update your profile information, click update when finished.

                            <form action="/api/v1/user" id="form">
                                <p><label for="first_name">First Name:</label></p>
                                <p><input type="text" id="first_name" name="first_name" /></p>

                                <p><label for="middle_name">Middle Name:</label></p>
                                <p><input type="text" id="middle_name" name="middle_name" /></p>

                                <p><label for="last_name">Last Name:</label></p>
                                <p><input type="text" id="last_name" name="last_name" /></p>

                                <p><label for="email">Email:</label></p>
                                <p><input type="text" id="email" name="email" /></p>

                                <p class="submit">
                                    <input type="submit" value="Update" />
                                <p>
                            </form>
                        </div>
                    </div>
                    <script>
                        // use the rest api to get the data
                        $.get("/api/v1/user/''' + web.ctx.session.email + '''", function(data) {
                            $('#first_name').val(data.first_name);
                            $('#middle_name').val(data.middle_name);
                            $('#last_name').val(data.last_name);
                            $('#email').val(data.email);
                        });

                        $("#form").submit(function(event) {

                            // stop form from submitting normally
                            event.preventDefault();

                            var post = '';
                            $(':input', '#form').each(function() {
                                if(this.value != 'Update') {
                                    if(this.value != '') {
                                        post += this.name + '=' + this.value + '&';
                                    }
                                }
                            });

                            post = post.substr(0,post.length-1);

                            alert(post);
                            
                            $.ajax({
                                type: 'PUT',
                                url: '/api/v1/user/''' + web.ctx.session.email + '''',
                                data: encodeURI(post),
                                statusCode: {
                                    404: function() {
                                        alert('Error updating profile');
                                    },
                                    200: function() {
                                        alert('Successfully updated profile');
                                    }
                                }
                            });
                        });
                    </script>
                </body>
                </html>
                '''

class upload:
    def GET(self):
        if web.ctx.session.login == 0:
            raise web.seeother('/login')
        else:
            return '''
                <!DOCTYPE html>
                <html>
                <head>
                    <script src="http://code.jquery.com/jquery-1.5.js"></script>
                    <link rel="stylesheet" href="static/css/theme.css">
                </head>
                <body>
                    <div id="header">
                        <div id="user">
                            Welcome, <b>''' + web.ctx.session.name + '''</b>
                        </div>
                        <div id="menu">
                            <a href="/">Home</a> |
                            <a href="/profile">Update Profile</a> |
                            <a href="/upload">Add Photo</a> |
                            <a href="/reset">Logout</a>
                        </div>
                    </div>

                    <div class="form">
                        <div>
                            Please give a name for your photo then browse to upload it.
                            <form method="POST" enctype="multipart/form-data" action="/api/v1/photo/">
                                <p><label for="name">File Name:</label></p>
                                <p><input type="text" id="name" name="name"/></p>

                                <p><label for="file">File:</label></p>
                                <p><input type="file" id="file" name="file" /></p>

                                <p class="submit">
                                    <input type="submit" value="Upload" />
                                </p>
                            </form>
                        </div>
                    </div>
                </body>
                </html>'''
