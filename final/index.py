__author__ = 'chadmaughan'

import web
from controller.photo import *
from controller.user import *

urls = (
    '/', 'index',
    '/api/v1/photo/(.*)', 'controller.photo.rest',
    '/api/v1/photo-list/(.*)', 'controller.photo.list',
    '/api/v1/user/(.*)', 'controller.user.rest',
    '/login', 'auth.login',
    '/reset', 'auth.reset',
    '/profile', 'forms.profile',
    '/upload', 'forms.upload',
    '/openid', 'web.webopenid.host'
)

app = web.application(urls, globals())

session = web.session.Session(app, web.session.DiskStore('sessions'), {'name':'', 'id':0, 'email':'', 'login':0})

def session_hook():
    web.ctx.session = session

app.add_processor(web.loadhook(session_hook))

class index:
    def GET(self):
        if web.ctx.session.login == 1:
            body = '''
                <!DOCTYPE HTML>
                <html lang="en-US">
                <head>
                    <meta charset="UTF-8">
                    <title>Gallery</title>
                    <script type="text/javascript">
                        global_url = "api/v1/photo-list/''' + str(web.ctx.session.id) + '''";
                    </script>
                </head>
                <body>
                    <div id="header">
                        <div id="user">
                            Welcome, <b>''' + web.ctx.session.name + '''</b>
                        </div>
                        <div id="menu">
                            <a href="/">Home</a> |
                            <a href="/profile">Update Profile</a> |
                            <a href="/upload">Add Photo</a> |
                            <a href="/reset">Logout</a>
                        </div>
                    </div>
                    <div id="gallery">
                    </div>
                    <script data-main="static/main" src="static/require.js"></script>
                </body>
                </html>
            '''
            return body
        else:
            raise web.seeother('/login')

if __name__ == "__main__":
    app.run()