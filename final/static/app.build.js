({
    appDir: "../",
    baseUrl: "static/",
    dir: "../../cs6890-final",
    paths:{
        'jquery': 'jquery'
    },
    modules: [
        { name: "main" }
    ],
    optimizeCss: "standard",
    inlineText: true
})