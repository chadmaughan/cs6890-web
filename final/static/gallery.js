define(['photo/photo'], function(Photo) {

    var hideleft = -380,
            left = -180,
            active = 300,
            right = 780,
            hideright = 960,
            container = $('#gallery'),
            photos = [];

    function deselectOtherPhotos(photo) {
        for (var i = 0; i < photos.length; i += 1) {
            if (photos[i] !== photo) {
                photos[i].deselect();
            }
        }
    }

    function populate(data, callback) {
        var i, id, obj, photo, fragment = document.createDocumentFragment();

        for (i = 0; i < data.length; i += 1) {
            id = i + 1;
            obj = data[i];
            photo = new Photo(id);
            photo.title.html(obj.name);
            photo.image.attr('src', 'static/images/' + obj.image);

            photos.push(photo);
            if (i > 2) {
                photo.element.css('left', hideright);
            }
            //add the element (not the jQuery wrapper) to the doc fragment
            fragment.appendChild(photo.element.get(0));
        }

        //arrange photos' starting positions
        leftPhoto = photos[0].element.css('left', left);
        rightPhoto = photos[2].element.css('left', right);

        //insert into DOM
        container.append(fragment);

        //execute the callback function
        callback();
    }

    function getPhoto(id) {
        for (var i = 0; i < photos.length; i += 1) {
            if (photos[i].id === parseInt(id)) {
                return photos[i];
            }
        }
    }

    function arrangePhotos(photo) {
        var index, card, i, length,
                newLeft = getPhoto(photo.id - 1),
                newRight = getPhoto(photo.id + 1);

        //apply the correct leftPosition to the newLeft, newRight, and photo
        if (newLeft) newLeft.element.css('left', left);
        photo.element.css('left', active);
        if (newRight) newRight.element.css('left', right);

        //hide left every card before the newLeft
        length = newLeft ? newLeft.id - 1 : 0;
        for (i = 0; i < length; i += 1) {
            card = photos[i];
            card.element.css('left', hideleft);
        }

        //hide right every card after the newRight
        index = newRight ? newRight.id : photos.length;
        for (index; index < photos.length; index += 1) {
            card = photos[index];
            card.element.css('left', hideright);
        }
    }

    //expose gallery module's public API
    return {
        deselectOtherPhotos: deselectOtherPhotos,
        arrangePhotos: arrangePhotos,
        populate: populate,
        getPhoto: getPhoto
    };
});