//Configure RequireJS
require({
    paths:{ 'jquery': 'jquery' },
    priority: ['jquery']
});

require(['jquery', 'utils/css', 'utils/pubsub', 'sammy', 'gallery', 'text!css/reset.css', 'text!css/theme.css'], function($, css, pubsub, sammy, gallery, reset, theme) {
    css.loadInternal(reset, 'css/reset.css', true);
    css.loadInternal(theme, 'css/theme.css');

    //get data and populate
    $.ajax({
        url: global_url,
        dataType: "json",
        success: function(data) {
            gallery.populate(data, setupHistory);
        }
    });

    //-----subscribe to photo-select event
    pubsub.on('photo-selected', function(photo) {
        //change the history hash, let the Sammy route do the rest
        window.location.hash = "/photo/" + photo.id;
    });

    function setupHistory() {
        var singlePageApp = $.sammy(function() {
            //select a photo based on the URL
            this.get('#/photo/:photo', function() {
                var photo = gallery.getPhoto(this.params['photo']);
                photo.select();
                gallery.deselectOtherPhotos(photo);
                gallery.arrangePhotos(photo);
            });
        });
        singlePageApp.run();
    }
});