define(['utils/css', 'utils/control', 'utils/pubsub', 'text!photo/photo.html', 'text!photo/photo.css'], function(css, control, pubsub, html, styles) {

    css.loadInternal(styles, 'photo/photo.css');

    function Photo(id) {
        this.id = id;
        this.element = $(html);
        this.selected = false;
        var instance = this;

        //wire up fields mapped to DOM elements
        control.mapFields(this, this.element, ['title', 'image']);

        //fire custom event when clicked
        this.element.click(function() {
            pubsub.fire('photo-selected', instance);
        });
    }

    Photo.prototype.select = function() {
        this.element.addClass('active');
        this.selected = true;
    };

    Photo.prototype.deselect = function() {
        if (this.selected) {
            this.element.removeClass('active');
            this.selected = false;
        }
    };

    return Photo;
});

