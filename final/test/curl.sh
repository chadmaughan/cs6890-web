#!/bin/bash

## test display user (GET)
curl -H "Accept: application/json" http://127.0.0.1:8000/api/v1/user/chadmaughan@gmail.com

## test create user (POST) --- note the trailing slash
curl -v -d "first_name=Larry&middle_name=Middle&last_name=Kudlow&email=larry@kudlow.com&password=test" http://127.0.0.1:8000/api/v1/user/

## test modify user (PUT)
curl -v -d "first_name=Larry&middle_name=Middle&last_name=Kudlow&email=larry@kudlow.com&password=test" http://127.0.0.1:8000/api/v1/user/
curl -v -X PUT -d "middle_name=Changed" http://127.0.0.1:8000/api/v1/user/larry@kudlow.com

## test delete user (DELETE)
curl -v -X DELETE http://127.0.0.1:8000/api/v1/user/larry@kudlow.com
